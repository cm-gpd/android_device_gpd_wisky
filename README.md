# Device configuration for GPD XD Plus

## Spec Sheet
| Feature                 | Specification                     |
| :---------------------- | :-------------------------------- |
| Chipset                 | MediaTek MT8173                   |
| CPU Architecture        | Cortex-A72 + Cortex-A53           |
| CPU Cores               | 2+4                               |
| Max CPU Frequency       | 2.1 GHz / 1.7 GHz                 |
| GPU Architecture        | IMG PowerVR GX6250                |
| Memory                  | 4GB RAM                           |
| Shipped Android Version | 7.0                               |
| Storage                 | 32GB                              |
| MicroSD                 | Up to 128GB                       |
| Dimensions              | 155 x 89 x 24 mm                  |
| Release Date            | April 1, 2018                     |

## Copyright

```
#
# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
```
