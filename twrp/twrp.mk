# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PRODUCT_COPY_FILES += device/gpd/wisky/twrp/twrp.fstab:recovery/root/etc/twrp.fstab

TW_THEME                     := landscape_hdpi
TARGET_RECOVERY_PIXEL_FORMAT := ABGR_8888
RECOVERY_TOUCHSCREEN_FLIP_Y  := true
RECOVERY_TOUCHSCREEN_SWAP_XY := true
TW_ROTATION                  := 270
TW_BRIGHTNESS_PATH           := /sys/class/leds/lcd-backlight/brightness
TW_MAX_BRIGHTNESS            := 255
TW_NO_CRYPTO                 := true
TW_NO_LEGACY_PROPS           := true
TW_USE_TOOLBOX               := true
