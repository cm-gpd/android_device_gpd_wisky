# ADB compatibility
PRODUCT_PROPERTY_OVERRIDES += \
   sys.usb.ffs.aio_compat=1

# Display
PRODUCT_PROPERTY_OVERRIDES += \
   ro.sf.lcd_density=240

# WiFi
PRODUCT_PROPERTY_OVERRIDES += \
   ap.interface=ap0 \
   wifi.direct.interface=p2p0 \
   wifi.interface=wlan0
