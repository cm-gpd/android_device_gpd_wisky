#
# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Architecture
TARGET_CPU_ABI := arm64-v8a
TARGET_CPU_ABI2 :=
TARGET_CPU_SMP := true
# SOC uses a72's, but a53/a57 options are compatible
TARGET_CPU_VARIANT := cortex-a53
TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv8-a
TARGET_2ND_CPU_VARIANT := cortex-a53.a57
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi

# Assert
TARGET_OTA_ASSERT_DEVICE := wisky

# Bluetooth
MTK_BT_SUPPORT := yes
MTK_BT_CHIP    := MTK_MT6630
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/gpd/wisky/comms

# Bootloader versions
#TARGET_BOARD_INFO_FILE := device/nvidia/wisky/board-info.txt

# Board
TARGET_BOARD_PLATFORM := mt8173
TARGET_NO_BOOTLOADER := true

# Combo chip
MTK_COMBO_SUPPORT := yes
MTK_COMBO_CHIP    := MT6630

# Display
SF_PRIMARY_DISPLAY_ORIENTATION := 270

# Kernel
TARGET_KERNEL_SOURCE    := kernel/mediatek/mt8173-common
TARGET_KERNEL_CONFIG    := mt8176_defconfig
BOARD_KERNEL_IMAGE_NAME := Image.gz-dtb
BOARD_KERNEL_CMDLINE    := bootopt=64S3,32N2,64N2
BOARD_KERNEL_BASE       := 0x40000000
BOARD_KERNEL_OFFSET     := 0x00080000
BOARD_RAMDISK_OFFSET    := 0x09000000
BOARD_TAGS_OFFSET       := 0x0e000000
BOARD_MKBOOTIMG_ARGS    := --base $(BOARD_KERNEL_BASE) --kernel_offset $(BOARD_KERNEL_OFFSET) --ramdisk_offset $(BOARD_RAMDISK_OFFSET) --tags_offset $(BOARD_TAGS_OFFSET)

# Manifest
DEVICE_MANIFEST_FILE := device/gpd/wisky/manifest.xml

# Partitions
BOARD_FLASH_BLOCK_SIZE             := 4096
BOARD_BOOTIMAGE_PARTITION_SIZE     := 16777216 # 16M
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 16777216 # 16M
BOARD_CACHEIMAGE_PARTITION_SIZE    := 1610612736 # 1.5G
BOARD_USERDATAIMAGE_PARTITION_SIZE := 26251096064 # ~24.45G
BOARD_SYSTEMIMAGE_PARTITION_SIZE   := 2684354560 # 2.5G
BOARD_VENDORIMAGE_PARTITION_SIZE   := 536870912 # 512M
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE  := ext4
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
TARGET_USERIMAGES_USE_EXT4         := true
TARGET_COPY_OUT_VENDOR             := vendor

# Recovery
TARGET_RECOVERY_FSTAB := device/gpd/wisky/initfiles/fstab.mt8173

# Treble
#PRODUCT_SHIPPING_API_LEVEL := 27
#BOARD_VNDK_VERSION         := current

# TWRP Support
ifeq ($(WITH_TWRP),true)
include device/gpd/wisky/twrp/twrp.mk
endif

# Wifi
MTK_WLAN_SUPPORT                 := yes
MTK_BASIC_PACKAGE                := yes
BOARD_WPA_SUPPLICANT_DRIVER      := NL80211
WPA_SUPPLICANT_VERSION           := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_mt66xx
BOARD_WLAN_DEVICE                := MediaTek
BOARD_HOSTAPD_DRIVER             := NL80211
BOARD_HOSTAPD_PRIVATE_LIB        := lib_driver_cmd_mt66xx
WIFI_DRIVER_STATE_CTRL_PARAM     := /dev/wmtWifi
WIFI_DRIVER_STATE_ON             := 1
WIFI_DRIVER_STATE_OFF            := 0
WIFI_DRIVER_FW_PATH_PARAM        := /dev/wmtWifi
WIFI_DRIVER_FW_PATH_STA          := STA
WIFI_DRIVER_FW_PATH_AP           := AP
WIFI_DRIVER_FW_PATH_P2P          := P2P

include vendor/mediatek/wisky/BoardConfigVendor.mk
