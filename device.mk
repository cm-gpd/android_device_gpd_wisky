#
# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET_MEDIATEK_VERSION := mt8173

# System properties
include $(LOCAL_PATH)/system_prop.mk

PRODUCT_CHARACTERISTICS  := tv
PRODUCT_AAPT_CONFIG      := xlarge large
PRODUCT_AAPT_PREF_CONFIG := xxhdpi
TARGET_SCREEN_HEIGHT     := 1280
TARGET_SCREEN_WIDTH      := 720

$(call inherit-product, frameworks/native/build/tablet-10in-xhdpi-2048-dalvik-heap.mk)

$(call inherit-product, vendor/mediatek/wisky/wisky-vendor.mk)

# Overlays
DEVICE_PACKAGE_OVERLAYS += \
    device/gpd/wisky/overlay

# Init related
PRODUCT_PACKAGES += \
    fstab.mt8173 \
    init.common_svc.rc \
    init.connectivity.rc \
    init.modem.rc \
    init.mt8173.usb.rc \
    init.project.rc \
    init.sensor_1_0.rc \
    init.mt8173.rc \
    init.recovery.mt8173.rc \
    ueventd.mt8173.rc

# Permissions
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.bluetooth.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.ethernet.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.ethernet.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml

# ATV specific stuff
ifeq ($(PRODUCT_IS_ATV),true)
    $(call inherit-product-if-exists, vendor/google/atv/atv-common.mk)

    PRODUCT_PACKAGES += \
        android.hardware.tv.input@1.0-impl
endif

# Audio
PRODUCT_PACKAGES += \
    AudioParamOptions.xml \
    audio_policy_configuration.xml \
    a2dp_audio_policy_configuration.xml \
    r_submix_audio_policy_configuration.xml \
    usb_audio_policy_configuration.xml \
    audio_policy_volumes.xml \
    default_volume_tables.xml

# Bluetooth
PRODUCT_PACKAGES += \
    libbt-vendor \
    android.hardware.bluetooth@1.0-service-mediatek \
    android.hardware.bluetooth@1.0-impl-mediatek

# Charger
PRODUCT_PACKAGES += \
    charger \
    charger_res_images

# Combo chip
TARGET_EXTRA_KERNEL_MODULES := wmt_drv bt_drv wmt_chrdev_wifi wlan_gen3_drv
PRODUCT_PACKAGES += \
    wmt_launcher \
    wmt_loader

# Health HAL
PRODUCT_PACKAGES += \
    android.hardware.health@2.0-service

# Light
PRODUCT_PACKAGES += \
    android.hardware.light@2.0-service-mediatek

# Media config
PRODUCT_COPY_FILES += \
    frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_audio.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_video_le.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_video_le.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml
PRODUCT_PACKAGES += \
    media_codecs.xml \
    media_codecs_performance.xml \
    media_profiles.xml

# USB
PRODUCT_PACKAGES += \
    android.hardware.usb@1.0-basic

# Wifi
PRODUCT_PACKAGES += \
    android.hardware.wifi@1.0-service-mediatek \
    hostapd \
    wificond \
    libwpa_client \
    wpa_supplicant \
    wpa_supplicant.conf \
    wpa_supplicant_overlay.conf \
    p2p_supplicant_overlay.conf
